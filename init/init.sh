#!/bin/bash
yum -y update
yum -y install epel-release
yum install -y python3 git
yum update &&  yum -y install curl wget net-tools iputils-ping sshpass
yum install -y sshpass

#Only the master needs ansible
if [[ $(hostname) = "master" ]]; then
   curl -sS https://bootstrap.pypa.io/pip/3.6/get-pip.py | sudo python3
  /usr/local/bin/pip3 install  --default-timeout=100 ansible
fi
#Change ssh connection method to support password authentification
sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
sudo systemctl restart sshd

#Update DNS in all machines
echo -e "192.168.2.30  master\n
        192.168.2.31 slave1\n" >> /etc/hosts