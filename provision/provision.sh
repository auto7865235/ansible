#!/usr/bin/env bash

# Create an ssh key

ssh-keygen -b 2048 -t rsa -f /home/vagrant/.ssh/id_rsa -q -N ""

# Copy the key to all the machines

for val in master slave1; do 
	echo "-------------------- COPYING KEY TO ${val^^} NODE ------------------------------"
	sshpass -p 'vagrant' ssh-copy-id -o "StrictHostKeyChecking=no" vagrant@$val 
done

# Generate working directory

PROJECT_DIRECTORY="/home/vagrant/working-directory/"
mkdir -p $PROJECT_DIRECTORY
cd $PROJECT_DIRECTORY
# Create the inventory and config file
echo -e "master\n\n[centos1]\nslave1\n" > inventory
echo -e "[defaults]\ninventory = inventory" > ansible.cfg
# running adhoc command to see if everything is fine
ansible all -i inventory -m "shell" -a "uptime"






